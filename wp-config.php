<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'EJCM' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0|eP0*6y!U)pS?Ba?ytK.W:,)V>l{zf{Nj0>rX>_C2jQ%D`+F[Q^>oo<Z?XY}TGd' );
define( 'SECURE_AUTH_KEY',  'qmUQFuh<lGLDDc0NX9}v_N<,+LJp.QQf,/[JIRjvLsA/>;@Sotneawr*7#zRCpxV' );
define( 'LOGGED_IN_KEY',    '%r6B]z&<.VAyp l(95UAuQ9%p(@oEJ0{Jt:u~*fy:rH_HfN<-^k[/?u4K@BBJF`R' );
define( 'NONCE_KEY',        '>$%Wa3h-&`<P&ll85/YZwAz]i!!kiCe7{l>!4TV(n+2.vA-kyCjUE>/B!D;2tZ=7' );
define( 'AUTH_SALT',        '8FsNv,700{e%Rh9@`WiT:/53{fo&)-b_XHI5AI?;g_q?2islQDP%F[e3TV(s?B?;' );
define( 'SECURE_AUTH_SALT', ' a~g{Gwl/- ,/a`LD!rkG?f3X5:5%vg+^bt)T 3;ESp%`{oi~ QWdelQdE|%a^%i' );
define( 'LOGGED_IN_SALT',   'kLC*DNsD.tDNUQcTsRO=?eT}vU1P9Yo`=jGWO.5 9x4)>@:<zb0%a!a`z>:D?TZV' );
define( 'NONCE_SALT',       'EuE,D4L#p-Nod.kS63Tr![DSS2o*<8W2j3R(CI]XOcb2MLku]:#Tq8bDTl4dSHXu' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
